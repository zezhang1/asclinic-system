import cv2
print(cv2.__version__)

#camSet = 'nvarguscamerasrc ! video/x-raw(memory:NVMM),width=1024, height=768, framerate=120/1, format=NV12 ! nvvidconv flip-method=0 ! nvegltransform ! nveglglessink -e'

camSet = 'nvarguscamerasrc sensor-id=0 ! video/x-raw(memory:NVMM), width=3264, height=2464, framerate=12/1, format=NV12 ! nvvidconv flip-method=0 ! video/x-raw, width = 800, height=600, format =BGRx ! videoconvert ! video/x-raw, format=BGR ! appsink'

#cam=cv2.VideoCapture('dev/video1')
cam=cv2.VideoCapture(camSet)
while True:
	_,frame = cam.read()
	cv2.imshow('myCam',frame)
	if cv2.waitKey(1) == ord('q'):
		break
cam.release()
cv2.destroyAllWindows()
