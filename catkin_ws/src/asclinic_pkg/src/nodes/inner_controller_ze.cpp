#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Int16.h"
// Declare "member" variables

ros::Publisher m_template_publisher_left;
ros::Publisher m_template_publisher_right;

float theta_l;
float theta_r;
float odom_theta_l;
float odom_theta_r;
float error_theta_l;
float error_theta_r;
float control_l;
float control_r;
int sign;
int count;
// Respond to subscriber receiving a messag

void theta_l_Callback(const std_msgs::Float32& msg)
{
	theta_l = msg.data;
}
void theta_r_Callback(const std_msgs::Float32& msg)
{
	theta_r = msg.data;
}
void odom_theta_l_Callback(const std_msgs::Float32& msg)
{
	odom_theta_l = msg.data;
}
void odom_theta_r_Callback(const std_msgs::Float32& msg)
{
	odom_theta_r = msg.data;
}
void Callback_sign(const std_msgs::Int16& msg)
{
//	ROS_INFO_STREAM("[MY_ODOM_RIGHT] Message receieved with data = " << msg.data);
	sign=msg.data;
}

int main(int argc, char* argv[])
{
	// Initialise the node

	ros::init(argc, argv, "inner_controller_ze");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle node_handle_for_global("/my_global_namespace");
	// Initialise a publisher
    m_template_publisher_left = node_handle_for_global.advertise<std_msgs::Int16>("set_motor_duty_cycle_left", 10, false);
    m_template_publisher_right = node_handle_for_global.advertise<std_msgs::Int16>("set_motor_duty_cycle_right", 10, false);
    ros::Subscriber my_sign = node_handle_for_global.subscribe("sign", 10, Callback_sign);

    // Initialise a subscriber
	ros::Subscriber theta_l_subscriber = node_handle_for_global.subscribe("theta_l", 1, theta_l_Callback);
	ros::Subscriber theta_r_subscriber = node_handle_for_global.subscribe("theta_r", 1, theta_r_Callback);
	ros::Subscriber odom_theta_l_subscriber = node_handle_for_global.subscribe("motor_delta_theta_left", 1, odom_theta_l_Callback);
	ros::Subscriber odom_theta_r_subscriber = node_handle_for_global.subscribe("motor_delta_theta_right", 1, odom_theta_r_Callback);
    
	ros::Rate loop_rate(100);
	while (ros::ok())
	{
		// Reading of the current sensor to be implemented here
   
		// Spin once so that this node can service any
		// callbacks that this node has queued.
		ros::spinOnce();
//		if (sign==1)
//	{
//	 theta_l=theta_l+odom_theta_l;
//	theta_r=theta_r+odom_theta_r;
//	}
//	if (sign==-1)
//	{
//	theta_l=theta_l-odom_theta_l;
//	theta_r=theta_r-odom_theta_r;
//	}
//	if (count ==10)
//	{
//	    ROS_INFO_STREAM( theta_l << " " << theta_r);
//	    theta_l=0;
//	    theta_r=0;
//	    count =0;
//	}
		
//		error_theta_l = theta_l - odom_theta_l;
//		error_theta_r = theta_r - odom_theta_r;
	
		    	control_l = theta_l;
		control_r = theta_r;
		
	//	ROS_INFO_STREAM("[MY_INNER_CONTROLLER] CONTROL_L = " << control_l<<" ODOM_THETA = "<<odom_theta_l);
        std_msgs::Int16 a;
	    a.data = control_l;
	    m_template_publisher_left.publish(a);

        std_msgs::Int16 b;
    	b.data = control_r;
    	m_template_publisher_right.publish(b);
		// Sleep for the specified loop rate
		loop_rate.sleep();
//		    count = count +1;
	} // END OF: "while (ros::ok())"
	
	
	// Initialise a timer
//	m_timer_for_publishing = node_handle_for_global.createTimer(ros::Duration(1.0), timerCallbackForPublishing, false);
	// Spin as a single-threaded node
	ros::spin();

	return 0;
}

