#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Int16.h"


// Declare "member" variables
ros::Timer m_timer_for_publishing;
ros::Publisher m_publisher_left;
ros::Publisher m_publisher_right;

// Respond to timer callback
//void timerCallbackForPublishing(const ros::TimerEvent&)
//{
//	static uint counter = 0;
//	counter++;

//}
int distance;
// Respond to subscriber receiving a messag
void Callback(const std_msgs::UInt16& msg)
{
//	ROS_INFO_STREAM("[MY_PUB_SUB_2] Message receieved with data = " << msg.data);
	distance = msg.data;
}
int main(int argc, char* argv[])
{
	// Initialise the node
	distance=0;
	ros::init(argc, argv, "my_pub");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle node_handle_for_global("/my_global_namespace");
	// Initialise a publisher
  m_publisher_left = node_handle_for_global.advertise<std_msgs::Int16>("set_motor_duty_cycle_left", 10, false);
   m_publisher_right = node_handle_for_global.advertise<std_msgs::Int16>("set_motor_duty_cycle_right", 10, false);
  
    // Initialise a subscriber
	//ros::Subscriber template_subscriber = node_handle_for_global.subscribe("gpio_event", 1, templateSubscriberCallback);
	ros::Subscriber my_subscriber = node_handle_for_global.subscribe("tof_distance", 1, Callback);
	ros::Rate loop_rate(100);
	ROS_INFO_STREAM("[MY_PUB_SUB_2] initial");
	while (ros::ok())
	{
		// Reading of the current sensor to be implemented here
   
		// Spin once so that this node can service any
		// callbacks that this node has queued.
	
    if (distance <=100)
    {
      std_msgs::Int16 a;
	a.data = 0;
	std_msgs::Int16 b;
	b.data = 0;
	m_publisher_left.publish(a);
	m_publisher_right.publish(b);
    }
      if (distance >100)
    {
  std_msgs::Int16 a;
	a.data = 10;
	std_msgs::Int16 b;
	b.data = 10;
m_publisher_left.publish(a);
	m_publisher_right.publish(b);
    }
    ros::spinOnce();
 //  ROS_INFO_STREAM("[MY_PUB_SUB_2] Message receieved");
		// Sleep for the specified loop rate
	loop_rate.sleep();
	} // END OF: "while (ros::ok())"
	
	
	// Initialise a timer
//	m_timer_for_publishing = node_handle_for_global.createTimer(ros::Duration(1.0), timerCallbackForPublishing, false);
	// Spin as a single-threaded node
	ros::spin();

	return 0;
}
