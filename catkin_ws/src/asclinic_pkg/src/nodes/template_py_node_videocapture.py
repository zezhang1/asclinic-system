#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
#
# This file is part of ASClinic-System.
#    
# See the root of the repository for license details.
#
# ----------------------------------------------------------------------------
#     _    ____   ____ _ _       _          ____            _                 
#    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________  
#   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \ 
#  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
# /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
#                                                 |___/                       
#
# DESCRIPTION:
# Template Python node with a publisher and subscriber to capture video
#
# ----------------------------------------------------------------------------





# Import the ROS-Python package
import rospy

# Import the standard message types
from std_msgs.msg import UInt32
from sensor_msgs.msg import Image

# Import the asclinic message types
from asclinic_pkg.msg import TemplateMessage

# Import opencv
import cv2

# Package to convert between ROS and OpenCV Images
from cv_bridge import CvBridge 





class TemplatePyNodeVideoCapture:

    def __init__(self):
        self.template_publisher = rospy.Publisher(node_name+"/template_topic", Image, queue_size=10)
        rospy.Subscriber(node_name+"/template_topic", Image, self.templateSubscriberCallback)
        # Gstreamer for capture video (sensor-id=0 for CAM0 and sensor-id=1 for CAM1)
        # This is not optimized !!! I just found somthing that works.
        self.camSet = 'nvarguscamerasrc sensor-id=0 ! video/x-raw(memory:NVMM), width=3264, height=2464, framerate=12/1, format=NV12 ! nvvidconv flip-method=0 ! video/x-raw, width = 800, height=600, format =BGRx ! videoconvert ! video/x-raw, format=BGR ! appsink'
        self.cam=cv2.VideoCapture(self.camSet)
        self.br = CvBridge()
        

    # Publish Image Message
    def PublishImage(self):
        while True:
            ret,frame = self.cam.read()
            if ret == True:
                rospy.loginfo('[TEMPLATE PY NODE VIDEO CAPTURE] publishing video frame')
                self.template_publisher.publish(self.br.cv2_to_imgmsg(frame))
            if cv2.waitKey(1) == ord('q'):
		        break

        

    # Respond to subscriber receiving a message
    def templateSubscriberCallback(self, msg):
        rospy.loginfo("[TEMPLATE PY NODE VIDEO CAPTURE] receiving video frame")
        # Convert ROS Image message to OpenCV image
        current_frame = self.br.imgmsg_to_cv2(msg)
        # Display image
        cv2.imshow("CAM 0", current_frame)
        cv2.waitKey(1)

if __name__ == '__main__':
    
    global node_name
    node_name = "template_py_node_videocapture"
    rospy.init_node(node_name, anonymous=False)
    template_py_node_videocapture = TemplatePyNodeVideoCapture()
    template_py_node_videocapture.PublishImage()
    template_py_node_videocapture.cam.release()
    cv2.destroyAllWindows()
