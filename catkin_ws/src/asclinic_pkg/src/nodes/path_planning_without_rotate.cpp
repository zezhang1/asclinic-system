#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Int16.h"
// Declare "member" variables

ros::Publisher m_x_publisher;
ros::Publisher m_y_publisher;
ros::Publisher m_phi_publisher;
ros::Timer time_publish;
ros::Publisher sign;
float desired_x;
float desired_y;
float desired_phi;
int count;



// Respond to subscriber receiving a messag


int main(int argc, char* argv[])
{
	// Initialise the node
	
    count =0;
	ros::init(argc, argv, "path_planning_without_rotate");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle node_handle_for_global("/my_global_namespace");
//	time_publish = node_handle_for_global.createTimer(ros::Duration(0.1), timerCallbackForPublishing, false);
	// Initialise a publisher
    m_x_publisher = node_handle_for_global.advertise<std_msgs::Float32>("desired_position_x", 1, false);
    m_y_publisher = node_handle_for_global.advertise<std_msgs::Float32>("desired_position_y", 1, false);
   m_phi_publisher = node_handle_for_global.advertise<std_msgs::Float32>("desired_position_phi", 1, false);
   sign = node_handle_for_global.advertise<std_msgs::Int16>("sign", 1, false);
    // Initialise a subscriber
	ros::Rate loop_rate(100);
	while (ros::ok())
	{
		// Reading of the current sensor to be implemented here
   
		// Spin once so that this node can service any
		// callbacks that this node has queued.
    if (count < 1000)
    {
	desired_x=1.45;
	desired_y=0;
	desired_phi=0;
//	ROS_INFO_STREAM("[MY_PATH PLANNING] count < 100 = " << count);
	std_msgs::Float32 a;
	a.data = desired_x;
	m_x_publisher.publish(a);

    	std_msgs::Float32 b;
	b.data = desired_y;
	m_y_publisher.publish(b);
	
		std_msgs::Float32 c;
	c.data = desired_phi;
	m_phi_publisher.publish(c);
	
	std_msgs::Int16 s;
	s.data = 1;
    sign.publish(s);
    }
    else if (count < 3500)
	 {
	desired_x=-1.45;
	desired_y=0;
	desired_phi=0;
//	ROS_INFO_STREAM("[MY_PATH PLANNING] count < 200 = " << count);
	std_msgs::Float32 a;
	a.data = desired_x;
	m_x_publisher.publish(a);

    	std_msgs::Float32 b;
	b.data = desired_y;
	m_y_publisher.publish(b);
	
		std_msgs::Float32 c;
	c.data = desired_phi;
	m_phi_publisher.publish(c);
	
	std_msgs::Int16 s;
	            s.data = -1;
               	sign.publish(s);
    }
    else if (count < 5000)
	 {	
	desired_x=0;
	desired_y=0;
	desired_phi=0;
//	ROS_INFO_STREAM("[MY_PATH PLANNING] count < 300 = " << count);
	std_msgs::Float32 a;
	a.data = desired_x;
	m_x_publisher.publish(a);

    	std_msgs::Float32 b;
	b.data = desired_y;
	m_y_publisher.publish(b);
	
		std_msgs::Float32 c;
	c.data = desired_phi;
	m_x_publisher.publish(c);
	
		std_msgs::Int16 s;
	            s.data = 1;
               	sign.publish(s);
    }
   
   
    ros::spinOnce();
		// Sleep for the specified loop rate
		if (count==5500)
        {
            count=0;
        }
		count = count + 1;
		loop_rate.sleep();
	} // END OF: "while (ros::ok())"
	
	
	// Initialise a timer
//	m_timer_for_publishing = node_handle_for_global.createTimer(ros::Duration(1.0), timerCallbackForPublishing, false);
	// Spin as a single-threaded node
	ros::spin();

	return 0;
}

