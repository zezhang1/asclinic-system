#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Int32.h"

// Declare "member" variables
ros::Timer m_timer_for_publishing;
ros::Publisher m_template_publisher;

// Respond to timer callback
//void timerCallbackForPublishing(const ros::TimerEvent&)
//{
//	static uint counter = 0;
//	counter++;

//}

// Respond to subscriber receiving a messag
bool pub = false;
void templateSubscriberCallback(const std_msgs::Int32& msg)
{
	ROS_INFO_STREAM("[MY_PUB_SUB] Message receieved with data = " << msg.data);
	if (msg.data==1)
	{
	pub = true;
	}
	if (msg.data==0)
	{
	pub = false;
	}
//	std_msgs::UInt32 a;
//	a.data = 100;
//	set_motor_duty_cycle_piublisher.publish(a);
}
int main(int argc, char* argv[])
{
	// Initialise the node
	
	pub = false;
	ros::init(argc, argv, "my_pub_sub");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle node_handle_for_global("/my_global_namespace");
	// Initialise a publisher
   m_template_publisher = node_handle_for_global.advertise<std_msgs::UInt16>("set_motor_duty_cycle", 10, false);
    // Initialise a subscriber
	ros::Subscriber template_subscriber = node_handle_for_global.subscribe("gpio_event", 1, templateSubscriberCallback);
	ros::Rate loop_rate(10);
	while (ros::ok())
	{
		// Reading of the current sensor to be implemented here
   
		// Spin once so that this node can service any
		// callbacks that this node has queued.
		ros::spinOnce();
    if (pub==true) 
    {
    std_msgs::UInt16 a;
	a.data = 50;
	m_template_publisher.publish(a);
    }
     if (pub==false) 
    {
    std_msgs::UInt16 a;
	a.data = 0;
	m_template_publisher.publish(a);
    }

		// Sleep for the specified loop rate
		loop_rate.sleep();
	} // END OF: "while (ros::ok())"
	
	
	// Initialise a timer
//	m_timer_for_publishing = node_handle_for_global.createTimer(ros::Duration(1.0), timerCallbackForPublishing, false);
	// Spin as a single-threaded node
	ros::spin();

	return 0;
}
