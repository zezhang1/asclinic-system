#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Int16.h"

// Declare "member" variables
ros::Publisher m_publisher_x;
ros::Publisher m_publisher_y;
ros::Publisher m_publisher_phi;
// Respond to timer callback

float delta_left;
float delta_right;
float r;
float b;
float x;
float y;
float phi;
float x_l;
float y_l;
float phi_l;
float delta_s;
float delta_phi;

float theta_l;
float theta_r;

int sign;


// Respond to subscriber receiving a messag
void Callback_left(const std_msgs::Float32& msg)
{
//	ROS_INFO_STREAM("[MY_ODOM_LEFT] Message receieved with data = " << msg.data);
	delta_left=msg.data;
}
void Callback_right(const std_msgs::Float32& msg)
{
//	ROS_INFO_STREAM("[MY_ODOM_RIGHT] Message receieved with data = " << msg.data);
	delta_right=msg.data;
}

void sign_Callback(const std_msgs::Int16& msg)
{
	sign = msg.data;
}

int main(int argc, char* argv[])
{
	// Initialise the node
   
	ros::init(argc, argv, "odom_ze_without_rotate");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle node_handle_for_global("/my_global_namespace");
	// Initialise a publisher
	m_publisher_x = node_handle_for_global.advertise<std_msgs::Float32>("current_odom_position_x", 1, false);
	m_publisher_y = node_handle_for_global.advertise<std_msgs::Float32>("current_odom_position_y", 1, false);
	m_publisher_phi = node_handle_for_global.advertise<std_msgs::Float32>("current_odom_position_phi", 1, false);
    // Initialise a subscriber
	ros::Subscriber my_subscriber_left = node_handle_for_global.subscribe("motor_delta_theta_left", 10, Callback_left);
	ros::Subscriber my_subscriber_right = node_handle_for_global.subscribe("motor_delta_theta_right", 10, Callback_right);
	ros::Subscriber sign_subscriber = node_handle_for_global.subscribe("sign", 1, sign_Callback);
	r = 0.05;
	b = 0.11;
	sign=1;
	ros::Rate loop_rate(100);
	while (ros::ok())
	{
    ros::spinOnce();
	delta_left=delta_left*sign;
	delta_right=delta_right*sign;
	delta_s=r*delta_left/2+r*delta_right/2;
	delta_phi=(r*delta_right/2/b)-(r*delta_left/2/b);
 // ROS_INFO_STREAM("[MY ODOM] ODOM position delta_left = " << r*delta_right/2/b << " y = " << r*delta_left/2/b << " delta_phi = " << delta_phi<< " phi = " << phi);	
	x=x_l+delta_s*cos(phi_l+delta_phi/2);
	y=y_l+delta_s*sin(phi_l+delta_phi/2);
	phi=phi_l+delta_phi;

	
	std_msgs::Float32 msg;
	msg.data = x;
    m_publisher_x.publish(msg);

	std_msgs::Float32 a;
	a.data = y;
	m_publisher_y.publish(a);
	
    std_msgs::Float32 b;
	b.data = phi;
	m_publisher_phi.publish(b);
	
    ROS_INFO_STREAM("[MY ODOM] ODOM position x = " << x << " y = " << y << " phi = " << phi);
	x_l=x;
	y_l=y;
	phi_l=phi;
	

	
	
	loop_rate.sleep();

	} // END OF: "while (ros::ok())"
	
	
	// Initialise a timer
	// Spin as a single-threaded node
	ros::spin();

	return 0;
}
