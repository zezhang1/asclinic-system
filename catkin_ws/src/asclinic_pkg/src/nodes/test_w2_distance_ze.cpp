#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Int16.h"
// Declare "member" variables
ros::Timer m_timer_for_publishing;
ros::Publisher m_publisher_left;
ros::Publisher m_publisher_right;
ros::Publisher sign;

// Respond to timer callback
//void timerCallbackForPublishing(const ros::TimerEvent&)
//{
//	static uint counter = 0;
//	counter++;


// Respond to subscriber receiving a messag
bool flag;
float delta_left;
float delta_right;
float total_theta_left;
float total_theta_right;
int count;

void Callback_left(const std_msgs::Float32& msg)
{
//	ROS_INFO_STREAM("[MY_ODOM_LEFT] Message receieved with data = " << msg.data);
	delta_left=msg.data;
}
void Callback_right(const std_msgs::Float32& msg)
{
//	ROS_INFO_STREAM("[MY_ODOM_RIGHT] Message receieved with data = " << msg.data);
	delta_right=msg.data;
}


int main(int argc, char* argv[])
{
	// Initialise the node
	count=0;
	flag = false;
	ros::init(argc, argv, "test_w2_distance_ze");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle node_handle_for_global("/my_global_namespace");
	// Initialise a publisher
    m_publisher_left = node_handle_for_global.advertise<std_msgs::Int16>("set_motor_duty_cycle_left", 1, false);
    m_publisher_right = node_handle_for_global.advertise<std_msgs::Int16>("set_motor_duty_cycle_right", 1, false);
    ros::Subscriber my_subscriber_left = node_handle_for_global.subscribe("motor_delta_theta_left", 10, Callback_left);
	ros::Subscriber my_subscriber_right = node_handle_for_global.subscribe("motor_delta_theta_right", 10, Callback_right);
	 sign = node_handle_for_global.advertise<std_msgs::Int16>("sign", 1, false);
    // Initialise a subscriber
	//ros::Subscriber template_subscriber = node_handle_for_global.subscribe("gpio_event", 1, templateSubscriberCallback);
	ros::Rate loop_rate(10);
//	ROS_INFO_STREAM("[MY_PUB_SUB_2] initial");
	while (ros::ok())
	{
		// Reading of the current sensor to be implemented here
   
		// Spin once so that this node can service any
		// callbacks that this node has queued.
	
   // if (count<20)
   // {
    std_msgs::Int16 a;
	a.data = 8;
	m_publisher_left.publish(a);
	std_msgs::Int16 b;
	b.data = 8;
	m_publisher_right.publish(b);
	std_msgs::Int16 s;
	s.data = -1;
	sign.publish(s);
//	ROS_INFO_STREAM("[COUNT<20] a = " << a << "b = " << b);
 //   }
//    else if (count<40)
///    {
//    std_msgs::Int16 a;
//	a.data = -21;
//	m_publisher_left.publish(a);
//	std_msgs::Int16 b;
//	b.data = -20;
//	m_publisher_right.publish(b);
//	std_msgs::Int16 s;
//	s.data = 1;
//	sign.publish(s);
//	ROS_INFO_STREAM("[COUNT<40] a = " << a << "b = " << b);
   //}
//    if (count==40)
//    {
 //   count=0;
 //   }
    ros::spinOnce();
    
 //  ROS_INFO_STREAM("[MY_PUB_SUB_2] Message receieved");
		// Sleep for the specified loop rate
	total_theta_left=delta_left+total_theta_left;
	total_theta_right=delta_right+total_theta_right;
	count = count +1;
	loop_rate.sleep();
	} // END OF: "while (ros::ok())"
	
	
	// Initialise a timer
//	m_timer_for_publishing = node_handle_for_global.createTimer(ros::Duration(1.0), timerCallbackForPublishing, false);
	// Spin as a single-threaded node
	ros::spin();

	return 0;
}

