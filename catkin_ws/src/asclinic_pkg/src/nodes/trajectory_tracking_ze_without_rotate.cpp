#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Int16.h"
#include "asclinic_pkg/ServoPulseWidth.h"
#include <bitset>
#include "pca9685/pca9685.h"
// Declare "member" variables

ros::Publisher m_theta_l_publisher;
ros::Publisher m_theta_r_publisher;
ros::Publisher m_template_publisher ;

float odom_x;
float odom_y;
float odom_phi;
float desired_x;
float desired_y;
float desired_phi;
float error_x;
float error_y;
float error_phi;
float control_v;
float control_o;
float theta_l;
float theta_r;
int count=0;



// Respond to subscriber receiving a messag

void x_Callback(const std_msgs::Float32& msg)
{
	odom_x = msg.data;
}
void y_Callback(const std_msgs::Float32& msg)
{
	odom_y = msg.data;
}
void phi_Callback(const std_msgs::Float32& msg)
{
	odom_phi = msg.data;
}
void desired_x_Callback(const std_msgs::Float32& msg)
{
	desired_x = msg.data;
}
void desired_y_Callback(const std_msgs::Float32& msg)
{
	desired_y = msg.data;
}
void desired_phi_Callback(const std_msgs::Float32& msg)
{
	desired_phi = msg.data;
}

int main(int argc, char* argv[])
{
	// Initialise the node
	count =0;

	ros::init(argc, argv, "trajectory_tracking_ze_without_rotate");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle node_handle_for_global("/my_global_namespace");
	// Initialise a publisher
    m_theta_l_publisher = node_handle_for_global.advertise<std_msgs::Float32>("theta_l", 10, false);
    m_theta_r_publisher = node_handle_for_global.advertise<std_msgs::Float32>("theta_r", 10, false);
    m_template_publisher  = node_handle_for_global.advertise<std_msgs::UInt16>("set_servo_pulse_width", 10, false);
    // Initialise a subscriber
	ros::Subscriber x_odom_subscriber = node_handle_for_global.subscribe("current_odom_position_x", 1, x_Callback);
	ros::Subscriber y_odom_subscriber = node_handle_for_global.subscribe("current_odom_position_y", 1, y_Callback);
	ros::Subscriber phi_odom_subscriber = node_handle_for_global.subscribe("current_odom_position_phi", 1, phi_Callback);
	ros::Subscriber x_desired_subscriber = node_handle_for_global.subscribe("desired_position_x", 1, desired_x_Callback);
	ros::Subscriber y_desired_subscriber = node_handle_for_global.subscribe("desired_position_y", 1, desired_y_Callback);
    ros::Subscriber phi_desired_subscriber = node_handle_for_global.subscribe("desired_position_phi", 1, desired_phi_Callback);
	ros::Rate loop_rate(100);
	while (ros::ok())
	{
		// Reading of the current sensor to be implemented here
   
		// Spin once so that this node can service any
		// callbacks that this node has queued.

		ros::spinOnce();
		error_x = desired_x - odom_x;
		error_y = desired_y - odom_y;
		error_phi = desired_phi - odom_phi;
		control_v = error_x;
		control_o = error_y;
	//	ROS_INFO_STREAM("[odom] control_o"<< odom_phi);
		if (count < 1000)
		 
		{
		   if (control_v>0)
		   {
		      if (error_y<0)
		      {
		          theta_l=23;
		          theta_r=20;
	    //     ROS_INFO_STREAM("[odom] a");
		      }
		      else if (error_y>0)
		      {
		          theta_l=20;
	  	          theta_r=23;
         //   ROS_INFO_STREAM("[odom] b");
		      }
		      else 
		      {
		          theta_l=21;
		          theta_r=20;
	  //  ROS_INFO_STREAM("[odom] c");

	  	      }
		   }
		    else 
		    {
		        theta_l=0;
		        theta_r=0;
		    }
		    	
		}
		
		else if(count < 1500)
		{

	  std_msgs::UInt16 b;
       b.data=2000;
     	m_template_publisher.publish(b);

		}
    	else if (count < 3500)
		 
		{
		   if (odom_x>-1.5)
		   {
		      if (error_y>0)
		      {
		          theta_l=-21;
		          theta_r=-23;
	   //     ROS_INFO_STREAM("[odom] a");
		      }
		      else if (error_y<0)
		      {
		          theta_l=-23;
	  	          theta_r=-20;
      //     ROS_INFO_STREAM("[odom] b");
		      }
		      else 
		      {
		          theta_l=-21;
	          theta_r=-20;
//	    ROS_INFO_STREAM("[odom] c");

	  	      }
		   }
		    else 
		    {
		        theta_l=0;
		        theta_r=0;
		    }
		    	
		}
		else if(count < 4000)
		{

	    std_msgs::UInt16 b;
        b.data=1000;
     	m_template_publisher.publish(b);

		}
		else if (count < 5000)
		 
		{
		   if (odom_x<0)
		   {
		      if (error_y<0)
		      {
		          theta_l=23;
		          theta_r=20;
	        ROS_INFO_STREAM("[odom] a");
		      }
		      else if (error_y>0)
		      {
		          theta_l=20;
	  	          theta_r=23;
         ROS_INFO_STREAM("[odom] b");
		      }
		      else 
		      {
		          theta_l=21;
	              theta_r=20;
	    ROS_INFO_STREAM("[odom] c");

	  	      }
		   }
		    else 
		    {
		        theta_l=0;
		        theta_r=0;
		    }
		    	
		}
		else 
		{
		    theta_l=0;
		    theta_r=0;
		   
		}
		std_msgs::Float32 a;
	    a.data = theta_l;
	    m_theta_l_publisher.publish(a);

        std_msgs::Float32 b;
    	b.data = theta_r;
    	m_theta_r_publisher.publish(b);
        if (count==5500)
        {
            count=0;
          
        }
        
		// Sleep for the specified loop rate
		count = count +1;
		loop_rate.sleep();
	} // END OF: "while (ros::ok())"
	
	
	// Initialise a timer
//	m_timer_for_publishing = node_handle_for_global.createTimer(ros::Duration(1.0), timerCallbackForPublishing, false);
	// Spin as a single-threaded node
	ros::spin();

	return 0;
}
